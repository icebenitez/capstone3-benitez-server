const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String, 
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
	},

	categoriesMade: [
		{
			categoryName: {
				type: String,
				required: true
			},
			categoryType: {
				type: String,
				required: true
			}
		}

	],

	records: [
		{				

			categoryType: {
				type: String,
				required: [true, "expense or income?"]
			},

			categoryName: {
				type: String,
				required: [true, "which category?"]
			},			
			amount: {
				type: Number,
				required: [true, "amount."]
			},
			description: {
				type: String,
				required: [true, "record name"]
			},
			dateMade: {
				type: Date,
				default: new Date()
			},
			balance: {
				type: Number,
				default: 0
			}

		}

	]
})

module.exports = mongoose.model('user', userSchema)