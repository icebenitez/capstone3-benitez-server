const jwt = require("jsonwebtoken")
const secret = "CourseBookingAPI" // secret can be any phrase

/*
JWT is like a gift wrapping service but with secrets.
Only the person with the secret can open the gift.
and if the wrapper has been tampered with, JWT also recognizes this
and disregards the gift.

meaning that if the token that provide user is has been
tampered with, or does not have the correct secret, they
should not be able to access certain functions in our app

*/

//Funtionality
//Create Access Token -> pack our gift and sign it with
//the secret.

module.exports.createAccessToken = (user) => {
	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}


//verify the token-> to check if the token is legit.

module.exports.verify = (req,res, next) => {

//we will put JWT in the headers of our request and because
//this function will send a response to client.

let token = req.headers.authorization
//this is in postman as: Authorization -> Bearer Token
//-> Token


	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)
		/*<string>.slice - cut the string starting from 
		the value up to specified value*/

		//start at the 7th and end at the last char

		/*the first 7 chars are not relevant/related to 
		the actual data we need*/

		return jwt.verify(token, secret, (err, data) => {
			/*if there is an error in the verification
			of the token, the verification fails*/
			return (err) ? res.send({auth: "failed"}): next()
			/*next() is a function that allows to proceed
			the next request*/

		})

	} else {
		//if a token is empty
		return res.send({auth: "failed"})
	
	}

}


//decode the token -> open the gift and unravel the contents
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) =>{

			return (err) ? null : 
			jwt.decode(token, {complete:true}).payload
			/*jwt.decode - decodes our token and gets 
			the payload*/
			/*payload in our case is the passed data
			from our createAccessToken*/

		})

	} else {

		return null

	}
}